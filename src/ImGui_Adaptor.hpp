#ifdef ImguiAdaptor_hpp
#error Multiple inclusion
#endif
#define ImguiAdaptor_hpp

#ifndef common_hpp
#error "Please include common.hpp before this file"
#endif

#ifndef HeapAllocator_hpp
#error "Please include HeapAllocator.hpp before this file"
#endif

#ifndef openglGame_common_hpp
#error "Please include openglGame_common.hpp before this file"
#endif

#ifndef gl3w_Functions_hpp
#error "Please include gl3w_Functions.hpp before this file"
#endif


#include "imgui.h"

namespace ImGui
{
  struct Adaptor
  {
    struct InputState
    {
      bool stoleMouse;
      bool stoleKeyboard;
    };

    static Adaptor *allocAndInit(HeapAllocator              *allocator,
                                 openglGame::DebugResources *debug_resources,
                                 gl3w::Functions            *gl);

    void shutdownAndFree(gl3w::Functions *gl);
    InputState newFrame(openglGame::Input *input);
    void render(gl3w::Functions *gl);
  };
}
