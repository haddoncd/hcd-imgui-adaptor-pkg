#ifdef ImGui_Adaptor_cpp
#error Multiple inclusion
#endif
#define ImGui_Adaptor_cpp

#ifndef ImGui_Adaptor_hpp
#include "ImGui_Adaptor.hpp"
#endif

#include <new>
#include "imgui.cpp"
#include "imgui_draw.cpp"
#include "imgui_demo.cpp"

namespace ImGui
{
  struct AdaptorInternal
  {
    ImGuiState   internalState;
    ImFontAtlas  fonts;
    GLuint       fontTexture;
    int          shaderHandle, vertHandle, fragHandle;
    int          attribLocationTex, attribLocationProjMtx;
    int          attribLocationPosition, attribLocationUV, attribLocationColor;
    unsigned int vboHandle, vaoHandle, elementsHandle;
  };

  Adaptor *Adaptor::allocAndInit(HeapAllocator              *allocator,
                                 openglGame::DebugResources *debug_resources,
                                 gl3w::Functions            *gl)
  {
    AdaptorInternal *state = alloc<AdaptorInternal>(allocator);

    SetInternalState(&state->internalState, true);

    ImGuiIO& io = GetIO();
    io.MemAllocFn = allocator->malloc;
    io.MemFreeFn  = allocator->free;
    io.SetClipboardTextFn = debug_resources->clipboard.copy;
    io.GetClipboardTextFn = debug_resources->clipboard.paste;
    io.ImeSetInputScreenPosFn = 0;

    io.IniFilename = 0;
    io.LogFilename = 0;

    {
      namespace Key = openglGame::KeyboardKey;
      io.KeyMap[ImGuiKey_Tab]        = Key::TAB;
      io.KeyMap[ImGuiKey_LeftArrow]  = Key::LEFT_ARROW;
      io.KeyMap[ImGuiKey_RightArrow] = Key::RIGHT_ARROW;
      io.KeyMap[ImGuiKey_UpArrow]    = Key::UP_ARROW;
      io.KeyMap[ImGuiKey_DownArrow]  = Key::DOWN_ARROW;
      io.KeyMap[ImGuiKey_PageUp]     = Key::PAGE_UP;
      io.KeyMap[ImGuiKey_PageDown]   = Key::PAGE_DOWN;
      io.KeyMap[ImGuiKey_Home]       = Key::HOME;
      io.KeyMap[ImGuiKey_End]        = Key::END;
      io.KeyMap[ImGuiKey_Delete]     = Key::DEL;
      io.KeyMap[ImGuiKey_Backspace]  = Key::BACKSPACE;
      io.KeyMap[ImGuiKey_Enter]      = Key::ENTER;
      io.KeyMap[ImGuiKey_Escape]     = Key::ESCAPE;
      io.KeyMap[ImGuiKey_A]          = Key::A;
      io.KeyMap[ImGuiKey_C]          = Key::C;
      io.KeyMap[ImGuiKey_V]          = Key::V;
      io.KeyMap[ImGuiKey_X]          = Key::X;
      io.KeyMap[ImGuiKey_Y]          = Key::Y;
      io.KeyMap[ImGuiKey_Z]          = Key::Z;
    }

    // Backup GL state
    GLint last_texture, last_array_buffer, last_vertex_array;
    gl->GetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
    gl->GetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
    gl->GetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);

    const GLchar *vertex_shader =
    "#version 330\n"
    "uniform mat4 ProjMtx;\n"
    "in vec2 Position;\n"
    "in vec2 UV;\n"
    "in vec4 Color;\n"
    "out vec2 Frag_UV;\n"
    "out vec4 Frag_Color;\n"
    "void main()\n"
    "{\n"
    " Frag_UV = UV;\n"
    " Frag_Color = Color;\n"
    " gl_Position = ProjMtx * vec4(Position.xy,0,1);\n"
    "}\n";

    const GLchar* fragment_shader =
    "#version 330\n"
    "uniform sampler2D Texture;\n"
    "in vec2 Frag_UV;\n"
    "in vec4 Frag_Color;\n"
    "out vec4 Out_Color;\n"
    "void main()\n"
    "{\n"
    " Out_Color = Frag_Color * texture( Texture, Frag_UV.st);\n"
    "}\n";

    state->shaderHandle = gl->CreateProgram();
    state->vertHandle = gl->CreateShader(GL_VERTEX_SHADER);
    state->fragHandle = gl->CreateShader(GL_FRAGMENT_SHADER);
    gl->ShaderSource(state->vertHandle, 1, &vertex_shader, 0);
    gl->ShaderSource(state->fragHandle, 1, &fragment_shader, 0);
    gl->CompileShader(state->vertHandle);
    gl->CompileShader(state->fragHandle);
    gl->AttachShader(state->shaderHandle, state->vertHandle);
    gl->AttachShader(state->shaderHandle, state->fragHandle);
    gl->LinkProgram(state->shaderHandle);

    state->attribLocationTex = gl->GetUniformLocation(state->shaderHandle, "Texture");
    state->attribLocationProjMtx = gl->GetUniformLocation(state->shaderHandle, "ProjMtx");
    state->attribLocationPosition = gl->GetAttribLocation(state->shaderHandle, "Position");
    state->attribLocationUV = gl->GetAttribLocation(state->shaderHandle, "UV");
    state->attribLocationColor = gl->GetAttribLocation(state->shaderHandle, "Color");

    gl->GenBuffers(1, &state->vboHandle);
    gl->GenBuffers(1, &state->elementsHandle);

    gl->GenVertexArrays(1, &state->vaoHandle);
    gl->BindVertexArray(state->vaoHandle);
    gl->BindBuffer(GL_ARRAY_BUFFER, state->vboHandle);
    gl->EnableVertexAttribArray(state->attribLocationPosition);
    gl->EnableVertexAttribArray(state->attribLocationUV);
    gl->EnableVertexAttribArray(state->attribLocationColor);

    gl->VertexAttribPointer(state->attribLocationPosition, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)offsetOf(ImDrawVert, pos));
    gl->VertexAttribPointer(state->attribLocationUV, 2, GL_FLOAT, GL_FALSE, sizeof(ImDrawVert), (GLvoid*)offsetOf(ImDrawVert, uv));
    gl->VertexAttribPointer(state->attribLocationColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(ImDrawVert), (GLvoid*)offsetOf(ImDrawVert, col));

    // Create Fonts Texture
    {
      io.Fonts = &state->fonts;
      new (io.Fonts) ImFontAtlas();

      // Build texture atlas
      unsigned char* pixels;
      int width, height;
      io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height);   // Load as RGBA 32-bits for OpenGL3 demo because it is more likely to be compatible with user's existing shader.

      // Create OpenGL texture
      gl->GenTextures(1, &state->fontTexture);
      gl->BindTexture(GL_TEXTURE_2D, state->fontTexture);
      gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      gl->TexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      gl->TexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);

      // Store our identifier
      io.Fonts->TexID = (void *)(intptr_t)state->fontTexture;

      // Cleanup (don't clear the input data if you want to append new fonts later)
      io.Fonts->ClearInputData();
      io.Fonts->ClearTexData();
    }

    // Restore modified GL state
    gl->BindTexture(GL_TEXTURE_2D, last_texture);
    gl->BindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
    gl->BindVertexArray(last_vertex_array);

    return (Adaptor *)state;
  }

  void Adaptor::shutdownAndFree(gl3w::Functions *gl)
  {
    AdaptorInternal *state = (AdaptorInternal *)this;

    if (state->vaoHandle) gl->DeleteVertexArrays(1, &state->vaoHandle);
    if (state->vboHandle) gl->DeleteBuffers(1, &state->vboHandle);
    if (state->elementsHandle) gl->DeleteBuffers(1, &state->elementsHandle);
    state->vaoHandle = state->vboHandle = state->elementsHandle = 0;

    gl->DetachShader(state->shaderHandle, state->vertHandle);
    gl->DeleteShader(state->vertHandle);
    state->vertHandle = 0;

    gl->DetachShader(state->shaderHandle, state->fragHandle);
    gl->DeleteShader(state->fragHandle);
    state->fragHandle = 0;

    gl->DeleteProgram(state->shaderHandle);
    state->shaderHandle = 0;

    ImGuiIO& io = GetIO();

    if (state->fontTexture)
    {
      gl->DeleteTextures(1, &state->fontTexture);
      io.Fonts->TexID = 0;
      state->fontTexture = 0;
    }

    void (*free)(void* ptr) = io.MemFreeFn;

    Shutdown();

    free(state);
  }

  Adaptor::InputState Adaptor::newFrame(openglGame::Input *input)
  {
    AdaptorInternal *state = (AdaptorInternal *)this;

    // Must init(...) before new_frame()
    assert(state->fontTexture);

    SetInternalState(&state->internalState, false);

    ImGuiIO& io = GetIO();

    // display size (every frame to accommodate for window resizing)
    io.DisplaySize = ImVec2((f32)input->windowSize_points.x, (f32)input->windowSize_points.y);
    io.DisplayFramebufferScale =
      ImVec2((f32)input->windowSize_pixels.x / input->windowSize_points.x,
             (f32)input->windowSize_pixels.y / input->windowSize_points.y);

    io.DeltaTime = input->timeStep_seconds;

    if (input->mouse.cursorPresent)
    {
      io.MousePos =
        ImVec2((float)input->mouse.cursorPos_points.x,
               (float)(input->windowSize_points.y - input->mouse.cursorPos_points.y));
    }
    else
    {
      io.MousePos = ImVec2(-1,-1);
    }

    {
      using namespace openglGame::MouseButton;

      Enum imgui_mouse_buttons[] =
      {
        LEFT,
        RIGHT,
        MIDDLE
      };

      for (int i = 0; i < arrayCount(imgui_mouse_buttons); ++i)
      {
        openglGame::ButtonState *button = &input->mouse.buttons[imgui_mouse_buttons[i]];
        io.MouseDown[i] = button->isDown || button->transitionCount >= 2;
      }
    }

    io.MouseWheel = input->mouse.scroll_wheelClicks;

    for (int i = 0; i < openglGame::KeyboardKey::ENUM_COUNT; ++i)
    {
      openglGame::ButtonState *key = &input->keyboard.keys[i];
      io.KeysDown[i] = key->isDown || key->transitionCount >= 2;
    }

    io.KeyCtrl  = io.KeysDown[openglGame::KeyboardKey::LEFT_CONTROL]
               || io.KeysDown[openglGame::KeyboardKey::RIGHT_CONTROL];
    io.KeyShift = io.KeysDown[openglGame::KeyboardKey::LEFT_SHIFT]
               || io.KeysDown[openglGame::KeyboardKey::RIGHT_SHIFT];
    io.KeyAlt   = io.KeysDown[openglGame::KeyboardKey::LEFT_ALT]
               || io.KeysDown[openglGame::KeyboardKey::RIGHT_ALT];

    io.AddInputCharactersUTF8((char *)input->keyboard.text.bytes_utf8);

    // Start the frame
    NewFrame();

    return InputState{io.WantCaptureMouse, io.WantCaptureKeyboard};
  }

  void Adaptor::render(gl3w::Functions *gl)
  {
    AdaptorInternal *state = (AdaptorInternal *)this;

    Render();
    ImDrawData *draw_data = GetDrawData();

    // Backup GL state
    GLint last_program; gl->GetIntegerv(GL_CURRENT_PROGRAM, &last_program);
    GLint last_texture; gl->GetIntegerv(GL_TEXTURE_BINDING_2D, &last_texture);
    GLint last_array_buffer; gl->GetIntegerv(GL_ARRAY_BUFFER_BINDING, &last_array_buffer);
    GLint last_element_array_buffer; gl->GetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &last_element_array_buffer);
    GLint last_vertex_array; gl->GetIntegerv(GL_VERTEX_ARRAY_BINDING, &last_vertex_array);
    GLint last_blend_src; gl->GetIntegerv(GL_BLEND_SRC, &last_blend_src);
    GLint last_blend_dst; gl->GetIntegerv(GL_BLEND_DST, &last_blend_dst);
    GLint last_blend_equation_rgb; gl->GetIntegerv(GL_BLEND_EQUATION_RGB, &last_blend_equation_rgb);
    GLint last_blend_equation_alpha; gl->GetIntegerv(GL_BLEND_EQUATION_ALPHA, &last_blend_equation_alpha);
    GLint last_viewport[4]; gl->GetIntegerv(GL_VIEWPORT, last_viewport);
    GLboolean last_enable_blend = gl->IsEnabled(GL_BLEND);
    GLboolean last_enable_cull_face = gl->IsEnabled(GL_CULL_FACE);
    GLboolean last_enable_depth_test = gl->IsEnabled(GL_DEPTH_TEST);
    GLboolean last_enable_scissor_test = gl->IsEnabled(GL_SCISSOR_TEST);

    // Setup render state: alpha-blending enabled, no face culling, no depth testing, scissor enabled
    gl->Enable(GL_BLEND);
    gl->BlendEquation(GL_FUNC_ADD);
    gl->BlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    gl->Disable(GL_CULL_FACE);
    gl->Disable(GL_DEPTH_TEST);
    gl->Enable(GL_SCISSOR_TEST);
    gl->ActiveTexture(GL_TEXTURE0);

    // Handle cases of screen coordinates != from framebuffer coordinates (e.g. retina displays)
    ImGuiIO& io = GetIO();
    float fb_width  = io.DisplaySize.x * io.DisplayFramebufferScale.x;
    float fb_height = io.DisplaySize.y * io.DisplayFramebufferScale.y;
    draw_data->ScaleClipRects(io.DisplayFramebufferScale);

    // Setup viewport, orthographic projection matrix
    gl->Viewport(0, 0, (GLsizei)fb_width, (GLsizei)fb_height);
    const float ortho_projection[4][4] =
    {
      { 2.0f/io.DisplaySize.x, 0.0f,                   0.0f, 0.0f },
      { 0.0f,                  2.0f/-io.DisplaySize.y, 0.0f, 0.0f },
      { 0.0f,                  0.0f,                  -1.0f, 0.0f },
      {-1.0f,                  1.0f,                   0.0f, 1.0f },
    };
    gl->UseProgram(state->shaderHandle);
    gl->Uniform1i(state->attribLocationTex, 0);
    gl->UniformMatrix4fv(state->attribLocationProjMtx, 1, GL_FALSE, &ortho_projection[0][0]);
    gl->BindVertexArray(state->vaoHandle);

    for (int n = 0; n < draw_data->CmdListsCount; n++)
    {
      const ImDrawList* cmd_list = draw_data->CmdLists[n];
      const ImDrawIdx* idx_buffer_offset = 0;

      gl->BindBuffer(GL_ARRAY_BUFFER, state->vboHandle);
      gl->BufferData(GL_ARRAY_BUFFER, (GLsizeiptr)cmd_list->VtxBuffer.size() * sizeof(ImDrawVert), (GLvoid*)&cmd_list->VtxBuffer.front(), GL_STREAM_DRAW);

      gl->BindBuffer(GL_ELEMENT_ARRAY_BUFFER, state->elementsHandle);
      gl->BufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)cmd_list->IdxBuffer.size() * sizeof(ImDrawIdx), (GLvoid*)&cmd_list->IdxBuffer.front(), GL_STREAM_DRAW);

      for (const ImDrawCmd* pcmd = cmd_list->CmdBuffer.begin(); pcmd != cmd_list->CmdBuffer.end(); pcmd++)
      {
        if (pcmd->UserCallback)
        {
          pcmd->UserCallback(cmd_list, pcmd);
        }
        else
        {
          gl->BindTexture(GL_TEXTURE_2D, (GLuint)(intptr_t)pcmd->TextureId);
          gl->Scissor((int)pcmd->ClipRect.x, (int)(fb_height - pcmd->ClipRect.w), (int)(pcmd->ClipRect.z - pcmd->ClipRect.x), (int)(pcmd->ClipRect.w - pcmd->ClipRect.y));
          gl->DrawElements(GL_TRIANGLES, (GLsizei)pcmd->ElemCount, sizeof(ImDrawIdx) == 2 ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT, idx_buffer_offset);
        }
        idx_buffer_offset += pcmd->ElemCount;
      }
    }

    // Restore modified GL state
    gl->UseProgram(last_program);
    gl->BindTexture(GL_TEXTURE_2D, last_texture);
    gl->BindBuffer(GL_ARRAY_BUFFER, last_array_buffer);
    gl->BindBuffer(GL_ELEMENT_ARRAY_BUFFER, last_element_array_buffer);
    gl->BindVertexArray(last_vertex_array);
    gl->BlendEquationSeparate(last_blend_equation_rgb, last_blend_equation_alpha);
    gl->BlendFunc(last_blend_src, last_blend_dst);
    if (last_enable_blend) gl->Enable(GL_BLEND); else gl->Disable(GL_BLEND);
    if (last_enable_cull_face) gl->Enable(GL_CULL_FACE); else gl->Disable(GL_CULL_FACE);
    if (last_enable_depth_test) gl->Enable(GL_DEPTH_TEST); else gl->Disable(GL_DEPTH_TEST);
    if (last_enable_scissor_test) gl->Enable(GL_SCISSOR_TEST); else gl->Disable(GL_SCISSOR_TEST);
    gl->Viewport(last_viewport[0], last_viewport[1], (GLsizei)last_viewport[2], (GLsizei)last_viewport[3]);
  }
}
